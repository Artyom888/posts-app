import React, {Component} from 'react';
import axios from 'axios';
import {API_URL,REQUEST_URL} from '../customConfigs/constants';
import { Modal, Button} from 'antd';
import { Input } from 'antd';
import validator from 'validator';

const { TextArea } = Input;


class EditModal extends Component {
  constructor(props){
    super(props);
 this.state = {
            title: '',
            desc: '',
            content: '',
            image:null,
            updatedPostId:'',
            Validate:{
                valid:true,
                fields:{
                    title : {
                        message : null,
                        valid : true
                    },
                    desc :  {
                        message : null,
                        valid : true
                    },
                    content : {
                        message : null,
                        valid : true
                    }
                },
            },
        };
       
    this.submitHandler = this.submitHandler.bind(this);
    this.handleChange = this.handleChange.bind(this);     
    this.validate = this.validate.bind(this);
  }

 componentWillReceiveProps(nextProps){
    this.setState({
      title: nextProps.updatedPost.title,
      desc: nextProps.updatedPost.desc,
      content: nextProps.updatedPost.content,
      updatedPostId: nextProps.updatedPost.id
    });
 }

validate(validField,value) {
        const newValidate  = this.state.Validate;
        value = value.trim();
      
      if(validator.isEmpty(value)){
            newValidate.fields[validField] = {
                valid : false,
                message : `${validField} must not be empty !`
            };
        }else if( value.length > 100 && validField !== 'content'){
            newValidate.fields[validField] = {
                valid : false,
                message : `${validField} must not be greater than 100 symbols !`
            }
        }else if(validField === 'email' && !validator.isEmail(value)){
             newValidate.fields[validField] = {
                valid : false,
                message : `${validField} must match the email template !`
            }
        }else{
            newValidate.fields[validField] = {
                valid:true,
                message:null
            }
        }
        this.setState({ Validate : newValidate },() => {
            let valid = true;
            Object.values(this.state.Validate.fields).map(function(field) {
                if(!field.valid ){
                   valid = false ;
                }
            });

            let updateStateValid = this.state.Validate;
            updateStateValid.valid = valid;
            this.setState({ 
              Validate:updateStateValid ,
            });
        });
    }

  handleChange(event) {
            this.validate(event.target.name,event.target.value);
            this.setState({[event.target.name]: event.target.value });          

    }

  submitHandler(event){
        let data = {
            "title": this.state.title,
            "desc": this.state.desc,
            "content": this.state.content
        }
    if(this.state.Validate.valid){
            axios.patch(API_URL + REQUEST_URL + '/'+ this.state.updatedPostId ,  data)
                  .then((response) => {
                    this.props.handleModalOk(response.data);  
                  })
                  .catch(function (error) {
                      console.log(error);
                  });
        }
  }

    render() { 
        return (
            <div>
                <Modal
                    visible={this.props.showModal.visible}
                    title="Post Update"
                    onOk={this.props.handleModalOk}
                    onCancel={this.props.handleModalCancel}
                    footer={[
                        <Button key="back"  onClick={this.props.handleModalCancel}>Return</Button>,
                        <Button key="submit" type="primary" loading={this.props.showModal.loading} onClick={this.submitHandler}>
                            Update
                        </Button>,
                    ]}
                >
                {
                  this.props.updatedPost &&  
                        <div className="example-input">
                        <label>Title:</label>
                            <Input size="large" name ="title"  onChange={(e) => this.handleChange(e)}  value={this.state.title} placeholder="Title" />
                           <small
                              className={`form-text text-muted alert alert-${this.state.Validate.fields.title.message && 'danger'}`}>
                              {this.state.Validate.fields.title.message}
                           </small>
                            <label>Description:</label>
                            <Input size="large" name ="desc" onChange={(e) => this.handleChange(e)}  value={this.state.desc} placeholder="Description" style={{ margin: '24px 0' }} />
                            <small
                              className={`form-text text-muted alert alert-${this.state.Validate.fields.desc.message && 'danger'}`}>
                              {this.state.Validate.fields.desc.message}
                           </small>
                           <label>Content:</label>
                          <TextArea  name ="content" onChange={(e) => this.handleChange(e)} value={this.state.content}  placeholder="Text content that will change" autosize={{ minRows: 3, maxRows: 6 }} />
                          <small
                              className={`form-text text-muted alert alert-${this.state.Validate.fields.content.message && 'danger'}`}>
                              {this.state.Validate.fields.content.message}
                           </small>

                         </div>
                }
                </Modal>
            </div>
        );
    }
}

export default EditModal;

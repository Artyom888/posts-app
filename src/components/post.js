import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../container/css/Posts.css';
import {API_URL,POSTS_IMG_URL,DEFAULT_POSTS_IMG_URL} from '../customConfigs/constants';
import {Card, Icon, Avatar} from 'antd';
const { Meta } = Card;


class PostBlock extends Component {
    render() { 
        let post = this.props.post;
        let postImg = API_URL + POSTS_IMG_URL;
        let postDefImg = DEFAULT_POSTS_IMG_URL;
    return <div key={post.id} className="posts">
                        <Link to={"/posts/" + post.id}>
                           <Card         
                               bodyStyle={{padding:10,margin:16}}                                          
                                cover={<img alt={post.title} className="postImg"
                                             src={post.images ? postImg + 'thumbs/' + post.images : postImg + postDefImg}/> 
                                      }
                                actions={[                                      
                                    <Icon  type="edit"  id={post.id} onClick={(e)=> this.props.editPostHandler(e,post.id)} />,
                                    <Icon  type="delete" id={post.id} onClick={(e) => this.props.deletePostHandler(e)} />
                               ]}
                            >
                            

                                <Meta
                                    avatar={<Avatar src={post.images ? postImg + 'thumbs/' + post.images : postImg + postDefImg}/>}
                                    title={post.title} 
                                    description={post.desc}
                                />
                            </Card>
                        </Link>
                </div>      
    }
}
export default PostBlock;

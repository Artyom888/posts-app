import React, {Component} from 'react';


class FormCreate extends Component {
    render() {
        return <form onSubmit={(e) => this.props.handleSubmit(e)} id="createForm" encType="multipart/form-data">
                <div className="form-group">
                    <label>Title:</label>
                    <input type="text" className="form-control form-control-lg " placeholder="Enter Title"
                           name="title" value={this.props.params.title}
                           onChange={(e) => this.props.handleChange(e)}/>
                    <small
                        className={`form-text text-muted alert alert-${this.props.params.Validate.fields.title.message && 'danger'}`}>
                        {this.props.params.Validate.fields.title.message}
                    </small>
                </div>
                <div className="form-group">
                    <label>Description:</label>
                    <input type="text" className="form-control form-control-lg" placeholder="Enter Description"
                           name="desc" value={this.props.params.desc}
                           onChange={(e) => this.props.handleChange(e)}/>
                    <small
                        className={`form-text text-muted alert alert-${this.props.params.Validate.fields.desc.message && 'danger'}`}>
                        {this.props.params.Validate.fields.desc.message}
                    </small>
                </div>
                <div className="form-group">
                    <label>Upload image:</label>
                    <input type="file" name="image" className="form-control-file"
                           onChange={(e) => this.props.handleFileChange(e)}/>
                    <small
                        className={`form-text text-muted alert alert-${this.props.params.Validate.fields.image.message && 'danger'}`}>
                        {this.props.params.Validate.fields.image.message}
                    </small>
                </div>
                <div className="form-group">
                    <label>Content: </label>
                    <textarea type="text" className="form-control form-control-lg" placeholder="Enter Content"
                           name="content" value={this.props.params.content}
                           onChange={(e) => this.props.handleChange(e)}/>
                    <small
                        className={`form-text text-muted alert alert-${this.props.params.Validate.fields.content.message && 'danger'}`}>
                        {this.props.params.Validate.fields.content.message}
                    </small>
                </div>
                <input type="submit" value="Send" className="btn btn-primary btn-lg"/>
            </form>
    }
}


export default FormCreate;
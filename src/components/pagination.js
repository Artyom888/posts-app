import React, {Component} from 'react';
export default function itemRender(current, type, originalElement) {
    if (type === 'prev') {
        return <a>Previous</a>;
    } else if (type === 'next') {
        return <a>Next</a>;
    }
    return originalElement;
}
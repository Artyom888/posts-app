import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Home from './container/Home';
import Posts from './container/posts/Posts';
import Create from './container/posts/Create';
import Show from './container/posts/Show';


class Routing extends Component {
    render(){
        return(
            <BrowserRouter >
                <div>
                    <Route exact path={'/'} component={ Home }/>
                    <Route exact path={'/posts'}  component={ Posts } />
                    <Route exact path={'/posts/:id'}  component={ Show } />
                    <Route exact path={'/create'} component={Create} />
                </div>
            </BrowserRouter>
        )

    }

}
export default Routing;

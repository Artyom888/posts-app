import React, {Component} from 'react';
import EditModal from '../../components/editModal';
import {API_URL,REQUEST_URL} from '../../customConfigs/constants';
import '../css/Posts.css';
import 'antd/dist/antd.css';
import axios from 'axios';
import TopMenu from '../../components/header';
import itemRender from '../../components/pagination';
import PostsList from '../../components/postsList';
import {Pagination} from 'antd';




class Posts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            pageTotalCount: null,
            pageDefSize: null,
            showModal:{
                visible:false,
                loading:false,    
            },
            updatePost:{},

        };
        this.onChangePagination = this.onChangePagination.bind(this);
        this.deletePostFromArray = this.deletePostFromArray.bind(this);
        this.postEditHandler = this.postEditHandler.bind(this);
        this.handleModalCancel = this.handleModalCancel.bind(this);
        this.handleModalOk = this.handleModalOk.bind(this);
    }

    componentDidMount() {
        axios.get(API_URL + REQUEST_URL, {
            params: {
                page: 1,
            }
        })
        .then((response) => {
            if(response.data.success){
                this.setState({
                    posts: response.data.posts,
                    pageTotalCount: parseInt(response.data.totalCount),
                    pageDefSize: parseInt(response.data.defaultPageSize),
                });
             }
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    onChangePagination(page) {
        axios.get(API_URL + REQUEST_URL, {
            params: {
                page: page,
            }
        })
        .then((response) => {
            this.setState({
                posts: response.data.posts,
            });
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    deletePostFromArray(id) {
        let posts = this.state.posts;
        let newPosts = posts.filter(function (elem) {
            return elem.id !== +id;
        });
        this.setState({
            posts: newPosts
        });
    }

    postEditHandler(event,postId){
        event.preventDefault();
        this.setState({showModal:{
            visible:true,
            loading:false,
          }
        });
        let updatePost; 
        Object.values(this.state.posts).find(function(field) {
            if( field.id === postId ){
              updatePost = field;
              return updatePost;
            }
        });
        this.setState({
            updatePost : updatePost
        });
        
    }

    handleModalCancel(){
        this.setState({
            showModal:{visible:false}
        });
    }

    handleModalOk(data){
        let postsAfterUpdate = this.state.posts.filter(function(post) {
           return post.id !== data.id;
        });   
        postsAfterUpdate.push(data);
        this.setState({
            updatePost: data,  
            posts:postsAfterUpdate,
            showModal:{
                visible:true,
                loading:true
            }    
        });
        setTimeout(() => {
            this.setState({ 
                showModal:{visible:false,loading:false}
             });
        }, 3000);                     

    }

    render() {
        return <div> 
            <TopMenu/>
            <div className="container">
                { this.state.posts && <PostsList posts={this.state.posts}
                                                  postDeleteHandler={this.deletePostFromArray}
                                                  editPostHandler={this.postEditHandler}/>
                }
            </div>

            <EditModal showModal = {this.state.showModal}
                        handleModalCancel = {this.handleModalCancel}
                        handleModalOk = {this.handleModalOk}
                        updatedPost = {this.state.updatePost ? this.state.updatePost : null } 
                        />
            <div>
                {   this.state.pageTotalCount >10 && 
                    <Pagination total={this.state.pageTotalCount} pageSize={this.state.pageDefSize}
                                onChange={this.onChangePagination} itemRender={itemRender}/>
                }
            </div>
        </div>

    }

}

export default Posts;

import React, {Component} from 'react';
import axios from 'axios';
import {API_URL,REQUEST_URL} from '../../customConfigs/constants';
import TopMenu from '../../components/header';
import FormCreate from '../../components/createForm';
import validator from 'validator';


class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            desc: '',
            content: '',
            Validate:{
                valid:false,
                fields:{
                    title : {
                        message : null,
                        valid : false
                    },
                    desc :  {
                        message : null,
                        valid : false
                    },
                    content : {
                        message : null,
                        valid : false
                    },
                    image : {
                        message : null,
                        valid : true
                    }
                },
            },
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.validate = this.validate.bind(this);
    }

    validate(validField,value) {
        const newValidate  = this.state.Validate;
        value = value.trim();
        if(validator.isEmpty(value)){
            newValidate.fields[validField] = {
                valid : false,
                message : `${validField} must not be empty !`
            };
        }else if( value.length > 100 && validField !== 'content'){
            newValidate.fields[validField] = {
                valid : false,
                message : `${validField} must not be greater than 100 symbols !`
            }
        }else{
            newValidate.fields[validField] = {
                valid:true,
                message:null
            }
        }
        this.setState({ Validate : newValidate },() => {
            let valid = true;
            Object.values(this.state.Validate.fields).map(function(field) {
                if( !field.valid){
                    valid = false ;
                }
            });

            let updateStateValid = this.state.Validate;
            updateStateValid.valid = valid;
            this.setState({ Validate:updateStateValid });
        });
    }

    handleChange(event) {
        this.validate(event.target.name,event.target.value);
        this.setState({[event.target.name]: [event.target.value]});

    }

    handleFileChange(event) {
        const newValidate  = this.state.Validate;
        if(validator.isEmpty(event.target.value)){
            newValidate.fields.image = {
                valid:true,
                message:null
            }
        }else if(!validator.matches(event.target.value, /.jpe?g|.gif|png/i)){
            newValidate.fields.image = {
                valid:false,
                message:'The invalid image format!'
            }
        }else{
            newValidate.fields.image = {
                valid:true,
                message:null
            }
        }


        this.setState({ Validate : newValidate },() => {
            let valid = true;
            Object.values(this.state.Validate.fields).map(function(field) {
                if( !field.valid){
                    valid = false ;
                }
            });

            let updateStateValid = this.state.Validate;
            updateStateValid.valid = valid;
            this.setState({ Validate:updateStateValid });
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        let formData = new FormData();
        formData.append("title", this.state.title);
        formData.append("desc", this.state.desc);
        formData.append("content", this.state.content);
        formData.append("image", event.target.image.files[0]);

        if(this.state.Validate.valid){
            axios.post(API_URL + REQUEST_URL, formData, {
                headers: {
                    'content-type': 'multipart/form-data',
                }
            })
            .then((response) => {
                if(response.data.success){
                    this.refs.alertMessage.className = 'alert alert-success';
                    this.refs.alertMessage.innerHTML = response.data.message;
                     this.setState({
                        title:'',
                        desc:'',
                        content:''
                     });   
                }else{
                    this.refs.alertMessage.className = 'alert alert-danger';
                    this.refs.alertMessage.innerHTML = response.data.message;
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        }else{
            this.refs.alertMessage.className = 'alert alert-danger';
            this.refs.alertMessage.innerHTML =  'Fill in the fields.' ;
        }
    }

    render() {
        return (
            <div>
                <TopMenu />
                <div className="container">
                <div  className="" ref='alertMessage' />
                    <div className="createForm">
                        <FormCreate
                            handleChange={this.handleChange}
                            handleFileChange={this.handleFileChange}
                            handleSubmit={this.handleSubmit}
                            params={this.state}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default Create;

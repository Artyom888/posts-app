import React, {Component} from 'react';
import '../css/Posts.css';
import TopMenu from '../../components/header';
import axios from 'axios';
import {API_URL,REQUEST_URL} from '../../customConfigs/constants';


const Post = (props) => {
let postImg = "http://magazine.loc/postsImages/";
let postDefImg = "defaultPhotos/defaultPhoto.jpg";
    return (
        <div>
            <div className="title">
                <h3 className="panel-title title">Post Title:{props.post.title}</h3>
            </div>
                <img className="showImg" src={(props.post.images ? postImg +'photos/' +  props.post.images : postImg + postDefImg)  }  alt={props.post.title}/>
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-desc desc">Post Description:</h3>
                    <p>{props.post.desc}</p>
                </div>
            </div>
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title "><b>Content</b></h3>
                </div>
                <div className="panel-body content">
                    <p>{props.post.content}</p>
                </div>
            </div>
        </div>
    )
};

class Show extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: []
        };
    }

    componentDidMount() {
        axios.get(`${API_URL+REQUEST_URL}/${this.props.match.params.id}`)
            .then((response) => {
                this.setState({post: response.data});
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return <div>
                    <TopMenu/>
                    <div className="container">
                        <Post post={this.state.post}/>
                    </div>
                </div>
    }
}

export default Show;

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import Routing from "./Routing";



ReactDOM.render(<Routing />, document.getElementById('root'));

